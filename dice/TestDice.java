package dice;

public class TestDice {

public static void main(String[] args) {
		testDie(6, 10000);
		testDie(12, 10000);
		testDie(20, 10000);
		
		testLoadedDie(6, 1, 50, 10000);
		testLoadedDie(12, 1, 40, 10000);
		testLoadedDie(20, 1, 30, 10000);
		
	}

	public static void testDie(int sides, int numberOfTimesToRoll){
		float[] probabilities;
		float[] counts;
		
		probabilities = new float[sides];
		counts = new float[sides];
		
		Die die = new Die();
		
		// TESTING THE DIE CLASS
		
		// Check the default die
		System.out.println("Die after construction, getTop: " + die.getTop());
		System.out.println("Die after construction, getNumberOfSides: " + die.getNumberOfSides());
		
		die = new Die(sides);
		
		// Check the die with specified number of sides
		System.out.println("Die after construction, getTop: " + die.getTop());
		System.out.println("Die after construction, getNumberOfSides: " + die.getNumberOfSides());
		
		int result = 0;
		//roll the die multiple times, recording the results in the "counts" array
		for(int i = 0; i < numberOfTimesToRoll; i++){
			die.roll();
			result = die.getTop();
			counts[result-1]++;
		}
		
		//use the "counts" array to calculate the measured probabilities of each face of the die
		for(int i = 0; i < sides; i++){
			probabilities[i] = counts[i] / numberOfTimesToRoll;
		}
		
		System.out.println(die);
		printCounts(counts);
		printProbabilities(probabilities);
	}
	
	public static void testLoadedDie(int sides, int loadedSide, int loadedFactor, int numberOfTimesToRoll){
		float[] probabilities;
		float[] counts;
		
		probabilities = new float[sides];
		counts = new float[sides];
		
		LoadedDie die = new LoadedDie();
		
		// TESTING THE DIE CLASS
		
		// Check the default die
		System.out.println("Die after construction, getTop: " + die.getTop());
		System.out.println("Die after construction, getNumberOfSides: " + die.getNumberOfSides());
		
		die = new LoadedDie(sides);
		
		// Check the die with specified number of sides
		System.out.println("Die after construction, getTop: " + die.getTop());
		System.out.println("Die after construction, getNumberOfSides: " + die.getNumberOfSides());
		
		die = new LoadedDie(sides, loadedSide, loadedFactor);
		
		int result = 0;
		//roll the die multiple times, recording the results in the "counts" array
		for(int i = 0; i < numberOfTimesToRoll; i++){
			die.roll();
			result = die.getTop();
			counts[result-1]++;
		}
		
		//use the "counts" array to calculate the measured probabilities of each face of the die
		for(int i = 0; i < sides; i++){
			probabilities[i] = counts[i] / numberOfTimesToRoll;
		}
		
		System.out.println("\n" + die);
		printCounts(counts);
		printProbabilities(probabilities);
	}
	
	// method for generically printing a float array without a name
	public static void printFloatArray(float[] array, int amount){
		System.out.println("\nPrinting " + amount + " elements of the array");
		for(int i = 0; i < amount; i++){
			System.out.print(array[i] + ", ");
		}
		System.out.println("\nPrinted " + amount + " elements of the array");
	}
	
	// method for generically printing a float array with a name
	public static void printFloatArray(float[] array, int amount, String name){
		System.out.println("\nPrinting " + amount + " elements of the array named: " + name);
		for(int i = 0; i < amount; i++){
			System.out.print(array[i] + ", ");
		}
		System.out.println("\nPrinted " + amount + " elements of the array named: " + name);
	}
	
	// method for printing the "counts" array with pretty formatting
	public static void printCounts(float [] array){
		System.out.println("\nPrinting die face counts");
		int total = 0;
		for(int i = 0; i < array.length; i++){
			System.out.println("Face " + (i+1) + " was rolled " + array[i] + " times");
			total+= array[i];
		}
		
		System.out.println("The die was rolled a total of " + total + " times");
	}
	
	// method for printing the "probabilities" array with pretty formatting
	public static void printProbabilities(float[] array){
		System.out.println("\nPrinting measured probabilities");
		float total = 0;
		for(int i = 0; i < array.length; i++){
			System.out.println("Face " + (i+1) + ": " + (array[i] * 100) + "%");
			total += (array[i] * 100);
		}
		
		System.out.println("Total: " + total);
	}
	
	

}
