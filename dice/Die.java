package dice;
import java.util.Random;

/** 
 * @author Nicklas Kenyon 09/30/2016
 * 
 * This class implements a die with a variable amount of sides (by default it is a 6-sided die)
 * and the functionality to roll the die and get the face that is currently facing "up"
 * as if it were laying on a table.
 * 
 */

public class Die implements Comparable<Die>{
	/**
	 * number of sides on the die
	 */
	protected int numSides;
	/**
	 * if the die were laying on a table, this side would be facing up
	 */
	protected int topFace;
	/**
	 * the random number generator that is used when rolling the die
	 */
	protected static Random rng = new Random();
	/**
	 * keeps track of whether or not the die has been rolled since it was last checked
	 */
	protected boolean hasRolled;
	
	/**
	 * Default constructor for the die class. A default die with 6 sides with the #6 side facing up
	 * is created.
	 */
	public Die(){
		// don't need to run a try/catch because 6 > 3
		this(6);
	}
	
	/**
	 * Constructor that allows you to specify the number of sides the die should have
	 * @param n the number of the sides the dice should have (ex. 6, 10, 12, 20)
	 */
	public Die(int n){
		if(n <=3){
			throw new IllegalArgumentException("The number of sides for a die must be greater than 3 (n > 3)");
		}
		this.numSides = n;
		this.topFace  = n;
		this.hasRolled = false;
	}
	
	/**
	 * This method returns the face on the die that is currently "facing up"
	 * as if it were laying on a table. It also resets the hasRolled field to false.
	 * 
	 * @return the side of the die currently facing up (this.topFace)
	 */
	public int getTop(){
		this.hasRolled = false;
		return this.topFace;
	}
	
	/**
	 * This method returns whether or not the die has been rolled since it was last checked.
	 * If the die hasn't been rolled since it was checked (using the getTop() method)
	 * then it will return false. Otherwise it will return true.
	 * 
	 * @return whether or not the die has been rolled since it was last checked (this.hasRolled)
	 */
	public boolean hasBeenRolled(){
		return this.hasRolled;
	}
	
	/**
	 * This method rolls the die. A random value within the range [1, numSides]
	 * is assigned to the topFace field. The hasRolled field is set to true.
	 */
	public void roll(){
		this.topFace = rng.nextInt(this.numSides)+1;
		this.hasRolled = true;
	}
	
	/**
	 * This method gives the number of sides on the die.
	 * @return number of sides on the die (this.numSides)
	 */
	public int getNumberOfSides(){
		return this.numSides;
	}
	
	/**
	 * Overrides method inherited from java.lang.Object. This method compares another die to
	 * the current die. and returns true if they have the same number of sides and the same number facing up.
	 * 
	 * @param obj An object of the Die class to test equality for with the current die.
	 * 
	 * @return true if the current die and the other die have the same number of sides and the same number facing up.
	 */
	@Override
	public boolean equals(Object obj){
		Die other = (Die) obj;
		return this.numSides == other.numSides
				&& this.topFace == other.topFace;
	}
	
	/**
	 * Overrides method inherited from java.lang.Object. This method returns a hash code for the
	 * die based on the number of sides it has and which number is currently facing up.
	 * 
	 * @return the number of sides squared, multiplied by the number currently facing up.
	 */
	@Override
	public int hashCode(){
		return this.numSides*this.numSides*this.topFace;
	}
	
	/**
	 * Overrides method inherited from java.lang.Object. This method translates the die's
	 * important values into an easy to understand string.
	 * 
	 * @return string that describes the die
	 */
	@Override
	public String toString(){
		return "Die with " + this.numSides + " sides and with the number " + this.topFace + " currently facing up.";
	}
	/**
	 * Overrides method inherited from java.lang.Comparable. This method compares the current die to another
	 * die and returns a value based on which die is greater. A die with more sides and/or a higher value for the side facing up
	 * will be considered greater than the other die and return a positive value. A die with fewer sides and/or a lower value for
	 * the side facing up will be considered less than the other die and return a negative value. If both die have the same number of sides
	 * and the same side facing up will be equal and return a value of 0.
	 * 
	 * @param o The die you wish to compare the current die to.
	 * 
	 * @return A negative value of the current die is less than the other die. 0 if both die are the same. A positive value if the current die
	 * is greater than the other die.
	 */
	@Override
	public int compareTo(Die o) {
		return this.hashCode() - o.hashCode();
	}

}
