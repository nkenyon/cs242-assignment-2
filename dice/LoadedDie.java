package dice;

/**
 * 
 * @author Nicklas Kenyon 09/30/16
 * 
 * This class extends from the Die class. It allows you to create a Die that favors a particular face,
 * and lets you choose the percentage chance that loaded side will be rolled.
 *
 */

public class LoadedDie extends Die{
	/**
	 * Which side of the die will be loaded. This should be a number between 1 and the number of sides on the die (inclusive)
	 */
	private int loadedSide;
	/**
	 * The percentage probability that the loaded side should be rolled.
	 */
	private int loadedFactor;
	
	/**
	 * Constructor that allows you to specify the number of sides on the die, which side should be loaded, and the percentage probability that the loaded side should be rolled.
	 * @param numberOfSides the number of sides that should be on the die
	 * @param loadedSide the number on the die that will have an unnatural probability
	 * @param loadedFactor the probability that the loaded side will be rolled
	 */
	public LoadedDie(int numberOfSides, int loadedSide, int loadedFactor){
		super(numberOfSides);
		if(loadedSide < 1 || loadedSide > numberOfSides){
			throw new IllegalArgumentException("The loaded side must be a value between 1 and the number of sides on the die. "
					+ "loadedSide given: " + loadedSide + ", numberOfSides given: " + numberOfSides);
		}
		if(loadedFactor < 0 || loadedFactor > 100){
			throw new IllegalArgumentException("The loaded factor is a percentage and must be between 0 (inclusive) and 100 (inclusive)."
					+ "The number you gave was: " + loadedFactor);
		}
		this.loadedSide = loadedSide;
		this.loadedFactor = loadedFactor;
	}
	
	/**
	 * Constructor that allows you to specify the number of sides on the die, but keeps all sides on the die with natural probabilities.
	 * @param numberOfSides the number of sides that should be on the die
	 */
	public LoadedDie(int numberOfSides){
		super(numberOfSides);
		this.loadedSide = 1;
		this.loadedFactor = 100/numberOfSides;
	}
	
	/**
	 * Default constructor that creates a 6-sided die with natural probabilities for all sides.
	 */
	public LoadedDie(){
		this(6);
	}
	
	/**
	 * Overrides the roll() method inherited from the Die class. Performs a percentage roll and checks to see if the loaded side was rolled.
	 * If not, a roll with even probabilities for the non-loaded sides is performed. This sets the topFace field to the rolled side.
	 */
	@Override
	public void roll(){
		int result = rng.nextInt(100)+1;
		if(result <= this.loadedFactor){
			this.topFace = this.loadedSide;
		}
		else{
			result = this.loadedSide;
			while(result == this.loadedSide){
				result = rng.nextInt(this.numSides)+1;
			}
			this.topFace = result;
		}
		this.hasRolled = true;
	}
	
	/**
	 * Overrides the equals method inherited from java.lang.Object. If the current die and the other die have the same hashCode, and therefore
	 * the same values, it returns true.
	 * 
	 * @param obj The object to compare to
	 * @return true if the two objects have the same values
	 */
	@Override
	public boolean equals(Object obj){
		LoadedDie other = (LoadedDie) obj;
		return this.hashCode() == other.hashCode();
	}
	
	/**
	 * Overrides the hashCode method inherited from java.lang.Object. The hash code is equal
	 * to the numberOfSides squared, times the topFace, times the loadedSide, times the loadedFactor.
	 * This creates a unique identifier for every possible state of the LoadedDie.
	 * 
	 * @return the unique hashCode for the Loaded Die.
	 */
	@Override
	public int hashCode(){
		return super.hashCode()*this.loadedSide*this.loadedFactor;
	}
	
	/**
	 * Overrides the toString method from java.lang.Object. The method returns a string that nicely describes the current LoadedDie.
	 * 
	 * @return a string that describes the current LoadedDie.
	 */
	@Override
	public String toString(){
		return "LoadedDie that has " + super.toString() + " The loaded side is: " + this.loadedSide + " and loadedFactor is: " + this.loadedFactor;
	}
	
	/**
	 * Overrides the compareTo method inherited from the Die class. The other Die is cast to a LoadedDie before comparison. 
	 * If the current LoadedDie has a greater hashCode than the other LoadedDie, then it returns a positive number.
	 * If the current LoadedDie has a lesser hashCode than the other LoadedDie, then the method returns a negative number.
	 * If the current LoadedDie has the same hashCode as the other LoadedDie, then it returns 0.
	 * 
	 * @param o a Die to compare the current die to.
	 * 
	 * @return an integer that is positive for greater than, 0 for equals, and negative for less than.
	 */
	@Override
	public int compareTo(Die o){
		LoadedDie other = (LoadedDie)o;
		return this.hashCode() - other.hashCode();
	}

}
