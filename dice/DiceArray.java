package dice;

import java.util.Arrays;

public class DiceArray {

	public static void main(String[] args) {
		LoadedDie[] dice = {
		                    new LoadedDie(12,1,50),
		                    new LoadedDie(12,1,45),
		                    new LoadedDie(12),
		                    new LoadedDie(12),
		                    new LoadedDie(12)
		};
		
		for(int i = 0; i < dice.length; i++){
			dice[i].roll();
			System.out.println(dice[i]);
		}
		Arrays.sort(dice);
		
		for(int i = 0; i < dice.length; i++){
			System.out.println(dice[i]);
		}

	}

}
